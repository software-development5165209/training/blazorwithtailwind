## Introduction

This is a simple pipeline example for a .NET Blazor Web Assembly with TailWind CSS V3, showing just
how easy it is to get up and running with .NET development using GitLab.

# Reference links

TailWind
- Chris Sainty's article: https://chrissainty.com/adding-tailwind-css-v3-to-a-blazor-app/
- Chris Sainty's github: https://github.com/chrissainty/talk-building-beautiful-blazor-apps-with-tailwind
- Chris Sainty's conferences: 
    - NDC London 2022: https://www.youtube.com/watch?v=GKbTgovP-VU
    - NDC Copenhagen 2022: https://www.youtube.com/watch?v=rlRNsCGQlOg
- Official Web: https://tailwindcss.com/
- https://play.tailwindcss.com/
- Installation: 
    - https://www.tailblazor.dev/
    - https://medium.com/@steven.kennis1/making-beautiful-native-apps-with-net-maui-blazor-and-tailwind-css-63bfe48a5bd5
    - https://umamaheswaran.com/2022/02/10/build-blazor-apps-with-tailwind-css/
- Bootstrap Vs tailwind:
    - https://blog.logrocket.com/comparing-tailwind-css-bootstrap-time-ditch-ui-kits/
    - https://2021.stateofcss.com/en-US/technologies/css-frameworks/
    - https://2022.stateofcss.com/en-US/css-frameworks/


- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [.NET Hello World tutorial](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/)

If you're new to .NET you'll want to check out the tutorial, but if you're
already a seasoned developer considering building your own .NET app with GitLab,
this should all look very familiar.

## What's contained in this project

The root of the repository contains the out of the `dotnet new console` command,
which generates a new console application that just prints out "Hello, World."
It's a simple example, but great for demonstrating how easy GitLab CI is to
use with .NET. Check out the `Program.cs` and `dotnetcore.csproj` files to
see how these work.

In addition to the .NET Core content, there is a ready-to-go `.gitignore` file
sourced from the the .NET Core [.gitignore](https://github.com/dotnet/core/blob/master/.gitignore). This
will help keep your repository clean of build files and other configuration.

Finally, the `.gitlab-ci.yml` contains the configuration needed for GitLab
to build your code. Let's take a look, section by section.

First, we note that we want to use the official Microsoft .NET SDK image
to build our project.

```
image: microsoft/dotnet:latest
```

We're defining two stages here: `build`, and `test`. As your project grows
in complexity you can add more of these.

```
stages:
    - build
    - test
```

Next, we define our build job which simply runs the `dotnet build` command and
identifies the `bin` folder as the output directory. Anything in the `bin` folder
will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

Similar to the build step, we get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```

This should be enough to get you started. There are many, many powerful options 
for your `.gitlab-ci.yml`. You can read about them in our documentation 
[here](https://docs.gitlab.com/ee/ci/yaml/).

## Developing with Gitpod

This template repository also has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

The `.gitpod.yml` ensures that, when you open this repository in Gitpod, you'll get a cloud workspace with .NET Core pre-installed, and your project will automatically be built and start running.
